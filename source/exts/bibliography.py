#
# Sphinx extension to flush bibliography forcely (for LaTeX)
#
# Original code written by tk0miya: https://gist.github.com/5886e3f5fb86bd312fd784a7a7b57884
#
# Usage:
#   1. Write `.. bibliography::` where you want to flash citation. In this case, we assume all citations are in `bib.rst`
#   2. put this file into `ext` directory
#   3. Modify conf.py. See also source/conf.py
#   4. Add `postface.rst` into your `index.rst` after citation rst
#

from docutils import nodes
from docutils.parsers.rst import Directive


class bibliography(nodes.Element):
    pass


class BibliographyDirective(Directive):
    def run(self):
        return [bibliography()]


def noop(self, node):
    pass


def visit_bibliography(self, node):

    # copied from visit_document() (sphinx.writers.latex)
    if self.bibitems:
        widest_label = ""  # type: unicode
        for bi in self.bibitems:
            if len(widest_label) < len(bi[0]):
                widest_label = bi[0]
        self.body.append(u'\n\\begin{sphinxthebibliography}{%s}\n' % widest_label)
        for bi in self.bibitems:
            target = self.hypertarget(bi[2] + ':' + bi[3],
                                      withdoc=False)
            self.body.append(u'\\bibitem[%s]{%s}{%s %s}\n' %
                             (self.encode(bi[0]), self.idescape(bi[0]),
                              target, bi[1]))
        self.body.append(u'\\end{sphinxthebibliography}\n')
        self.bibitems = []

    raise nodes.SkipNode


def setup(app):
    app.add_node(bibliography,
                 html=(noop, noop),
                 latex=(visit_bibliography, None))

    app.add_directive('bibliography', BibliographyDirective)